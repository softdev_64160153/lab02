/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2;

/**
 *
 * @author mookda
 */

import java.util.Scanner;

public class Lab2 {

    static char[][] board = new char[3][3];
    private static char currentPlayer = 'X';
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        boolean playAgain = true;
        printWelcome();

        while (playAgain) {
            initializeBoard();

            while (true) {
                printTable();
                printTurn();
                inputMove();
                if (isWinner()) {
                    printTable();
                    printWinner();
                    break;
                }
                if (isDraw()) {
                    printTable();
                    printDraw();
                    break;
                }
                switchTurn();
            }

            playAgain = playAgain();
        }

        System.out.print("♥ Thanks for playing ♥");

    }

    public static void printWelcome() {
        System.out.println("Welcome to OX");
        System.out.println("!Game Start!");

    }

    private static void initializeBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = ' ';
            }
        }
    }

    public static void printTable() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (j == 0) {
                    System.out.print("| " + board[i][j] + " ");
                } else if (j == 2) {
                    System.out.print(" " + board[i][j] + " |");
                } else {
                    System.out.print("| " + board[i][j] + " |");
                }
            }
            System.out.println("\n-------------");
        }
    }

    public static void printTurn() {
        System.out.println("- Player '" + currentPlayer + "' Turn -");
    }

    private static void inputMove() {
        System.out.print("Please enter your move row[1-3] col[1-3]: ");
        String move = sc.nextLine();

        if (!move.matches("\\d \\d")) {
            System.out.println("Invalid format, Please enter your move again.");
            inputMove();
            return;
        }

        int row = Character.getNumericValue(move.charAt(0))-1;
        int col = Character.getNumericValue(move.charAt(2))-1;

        if (!isValidMove(row, col)) {
            System.out.println("Invalid move, Please try again.");
            inputMove();
            return;
        }

        board[row][col] = currentPlayer;
    }

    private static boolean isValidMove(int row, int col) {
        return row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == ' ';
    }

    private static void switchTurn() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    private static void printWinner() {
        System.out.println("Congrats! Player " + currentPlayer + " wins.");
    }

    private static boolean isWinner() {
        return checkRow() || checkCol() || checkX1() || checkX2();
    }

    private static void printDraw() {
        System.out.println("It's a draw!");
    }

    public static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == ' ') {
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == currentPlayer && board[i][1] == currentPlayer && board[i][2] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (board[0][i] == currentPlayer && board[1][i] == currentPlayer && board[2][i] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkX1() {
        if (board[0][0] == currentPlayer && board[1][1] == currentPlayer && board[2][2] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean checkX2() {
        if (board[0][2] == currentPlayer && board[1][1] == currentPlayer && board[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean playAgain() {
        System.out.print("Do you wanna play again? (Y/N): ");
        String choice = sc.nextLine().toUpperCase();

        while (!choice.equals("Y") && !choice.equals("N")) {
            System.out.print("Invalid choice, Do you wanna play again? (Y/N): ");
            choice = sc.nextLine().toUpperCase();
        }

        return choice.equals("Y");
    }

}
